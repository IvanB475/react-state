import React, { useState } from "react";


export default class Counter extends React.Component {
    state = { value: 0};

    componentDidMount() {
       this.counterInterval = setInterval(() => {
            this.handleCounter()
        }, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.counterInterval);
    }

    handleCounter() {
        this.setState({value: this.state.value + 1});
    }

    render() {
        return (
            <h1>Counter is currently at: {this.state.value || 0}</h1>
        )
    }
}